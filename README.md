### Storage Space

#### Description
PowerShell sscript for Zabbix to monitor state storage space and physical disk.

#### Install

1. Copy file or content file <b>UserParameters.conf</b> in Zabbix directory on you monitored server (Default C:\Program Files\Zabbix Agent\zabbix_agentd.conf.d\).

2. Copy file <b>Get-StorageSpace.ps1</b> in folder C:\script\.

3. Import <b>zbx_tmpl_WindowsStorageSpace.xml</b> in you Zabbix server.

4. Attach Zabbix template <b>Windows Storage Space</b> on host.

#### Screenshot
##### Physical Disk monitoring
![Physical Disk monitoring](./screenshots/Disk.png)

##### Storage Space Monitoring
![Storage Space Monitoring](./screenshots/Storage.png)
