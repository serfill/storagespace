Param(
    [Parameter(Mandatory = $true)][string]$ActionType,
    [parameter(Mandatory = $true)][string]$Key,
    [parameter(Mandatory = $false)][string]$Value
)

$ActionType = $ActionType.ToLower().Replace("'", "")
$Key = $Key.ToLower().Replace("'", "")
$Value = $Value.ToLower().Replace("'", "")

[Console]::OutputEncoding = [System.Text.Encoding]::UTF8

if ($ActionType -eq 'discover') {
    if ($Key -eq 'pool') {
        $result_json = [pscustomobject]@{
            'data' = @(
                foreach ($i in Get-StoragePool -IsPrimordial $false -ErrorAction SilentlyContinue) {
                    [pscustomobject]@{'{#POOL}' = $i.FriendlyName }
                }
            )
        } | ConvertTo-Json
        [console]::WriteLine($result_json)
    }

    if ($Key -eq 'disk') {
        $result_json = [pscustomobject]@{
            'data' = @(
                foreach ($i in Get-PhysicalDisk) {
                    [pscustomobject]@{
                        '{#DISK}'  = $i.SerialNumber
                        '{#MODEL}' = $i.Model
                    }
                }
            )
        } | ConvertTo-Json
        [console]::WriteLine($result_json)
    }
}

if ($ActionType -eq 'get') {
    if ($Value -eq 'pool') {
        $Storage = Get-StoragePool -FriendlyName $Key
        $State = if ($Storage.HealthStatus -eq "Healthy") { 1 } else { 0 }
        $result_json = [PSCustomObject]@{
            State                        = $State
            OperationalStatus            = $Storage.OperationalStatus
            RepairPolicy                 = $Storage.RepairPolicy
            Version                      = $Storage.Version
            SupportsDeduplication        = $Storage.SupportsDeduplication
            ResiliencySettingNameDefault = $Storage.ResiliencySettingNameDefault
        } | ConvertTo-Json
        [Console]::WriteLine($result_json)       
    }

    if ($Value -eq 'disk') {
        $disk = Get-PhysicalDisk -SerialNumber $Key
        $State = if ($disk.HealthStatus -eq "Healthy") { 1 } else { 0 }   
        $result_json = [pscustomobject]@{
            OperationalStatus = $disk.OperationalStatus
            BusType           = $disk.BusType
            State             = $State
            MediaType         = $disk.MediaType
            Model             = $disk.Model
            PhysicalLocation  = $disk.PhysicalLocation
            FirmwareVersion   = $disk.FirmwareVersion
        } | ConvertTo-Json
        
        [console]::WriteLine($result_json)
    }
}